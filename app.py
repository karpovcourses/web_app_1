#!/usr/bin/python3
import sys
import time
import flask
import datetime

ts = str(datetime.datetime.now())

app = flask.Flask(__name__)

port = int(sys.argv[1])

@app.route('/')
def index():
    return f"Ansible version 4 from {ts}"

app.run('0.0.0.0', port)
